"""
function compliance(V,i)
Compliance computations
V : current compartment volume
i : index of the compartment
"""
function compliance(Vi,i)

    # Initialization
    C = 6.5e-4

    # Computation of the compliance according to various models (uncomment the chosen one)
    #C = C_1(Vi)
    C = C_2(Vi)
    #C = C_3(Vi)

    C = C*coeff[i]
    
    return C
end

"""
function elastance(V)
Elastance computation
V : current compartment volume
i : index of the compartment
"""
function elastance(Vi)

    C = 6.5e-4
    C = C_2(Vi)
    E = 1 / C

    return E
end

"""
function C_1(V)
Compliance computation according to the model used in Swan et al. (2012)
V : current compartment volume
"""
function C_1(Vi)
    @eval using .Param

    V0 = Param.V_FRC/2^N
    
    if Vi>0
        lambda = (Vi/V0)^(1/3)
    else
        lambda = 1e-6
    end
    lambda_star = 1.15
    gamma = 3/4*(3*Param.a+Param.b)*(lambda_star^2-1)^2
    C_star = Param.ksi*exp(gamma)/(6*V0)*(3*(3*Param.a+Param.b)^2*
                (lambda_star^2-1)^2/lambda_star^2+(3*Param.a+Param.b)*
                (lambda_star^2+1)/lambda_star^4)
    C_star = 1/C_star
    if lambda < lambda_star
        C = C_star/lambda_star*lambda
    else
        gamma = 3/4*(3*Param.a+Param.b)*(lambda^2-1)^2
        C = Param.ksi*exp(gamma)/(6*V0)*(3*(3*Param.a+Param.b)^2*(lambda^2-1)^2/lambda^2+(3*Param.a+Param.b)*(lambda^2+1)/lambda^4)
        C = 1/C
        
    end
    C = C/0.010197 #conversion to cmH2O.L-1
    return C
end

"""
function C_2(V)
Compliance computation according to the model from Birzle et al. (2019)
V : current compartment volume
"""
function C_2(Vi)
    @eval using .Param

    V0 = Param.V_FRC / 2^N

    # Stretch, assuming a homogenous isotropic deformation (F = diag(lambda))
    lambda = (Vi / V0)^(1 / 3) 

    # Expression of the elastance, assuming a strain energy function as in Birzle et al. (2019)
    E = 6 *Param.c - (30 * Param.c) / (lambda^2 * (lambda^6)^Param.beta) +
        (36 * Param.c * lambda^10 * (Param.beta + 1)) / (lambda^6)^(Param.beta + 2) + 
        (10 * Param.c3 * Param.d3 * lambda^4 * ((lambda^6)^(1 / 3) - 1)^(Param.d3 - 1)) / lambda^4 - 
        (8 * Param.c3 * Param.d3 * lambda^10 * (lambda^2 - 1)^(Param.d3 - 1)) / (lambda^6)^(5 / 3) + 
        (4 * Param.c3 * Param.d3 * lambda^10 * (lambda^2 - 1)^(Param.d3 - 2) * (Param.d3 - 1)) / lambda^8

    # Derivative with respect to the volume instead of strech (chain rule)
    E /= 3*lambda^2*V0

    C = 1 / E
    C *= 1#calibration coefficient
    C = C / 0.010197 #conversion to cmH2O.L-1

    return C
end

"""
function C_3(V)
Compliance computation according to the model used in Martin & Maury (2013)
V : current compartment volume
"""
function C_3(Vi)

    @eval using .Param

    V_TLC = Param.V_TLC/2^Param.N#total lung capacity volume
    V_FRC = Param.V_FRC/2^Param.N # functional residual capacity volume
    V_RV = Param.V_RV/2^Param.N # residual volume
    a = 1/(V_TLC-V_FRC)^2
    b= 1/(V_FRC-V_RV)^2
    
    lambda = 2^Param.N*Param.E0/(a+b)
    
    C = (1/lambda)*((V_TLC-Vi)^2*(Vi-V_RV)^2)/((Vi-V_RV)^2+(V_TLC-Vi)^2)
    return C

end

"""
function coeff_comp(csv_file)
Read the csv file containing the coefficients used to weight the compliance of a compartment 
according to the HU units of the corresponding voxels in the CT-scans.
"""
function coeff_comp(csv_file)
    
    data_csv = CSV.File(csv_file, header = 0; types = Float64)

    if size(data_csv)[1] != 2^Param.N
        throw(DomainError(size(data_csv)[1], "Coefficient must be of size 2^N"))
    end
    coeff = zeros(2^N, 1)
    coeff = data_csv.Column1
    
    return coeff
end