# README #

### What is this repository for? ###

This repository includes a simplified lung model developed to evaluate the potential benefits of placing patients suffering from COVID19-related ARDS in prone position

### How do I get set up? ###

* This model is implemented using the Julia language, so it has to be installed before being able to run it
* The code requires the installation of the following packages *Statistics*, *Plots* and *PlotlyJS*

### Contact ###

* Author : *Claire* claire.bruna-rosso@univ-eiffel.fr  
All comments, suggestions, feedback are very welcomed!
