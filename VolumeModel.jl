#=Volume driven model
Controlled by the flow delivered by the ventilator
=#

module VolModel

include("Parameters.jl")
using .Param

N = Param.N

t = zeros(floor(Int, Param.endtime / Param.delta_t))

for i = 2:length(t)
   t[i] = t[i-1] + Param.delta_t
end

include("Geometry.jl")
ball_repart = zeros(3,1)
ball_repart = ballrepart(N)

nx = ball_repart[1]
ny = ball_repart[2]
nz = ball_repart[3]

using Statistics
y_eso = Statistics.mean(ball_coords[2, :])

include("Inputs.jl")

V_init = Param.V_FRC .* Param.V_repart
V = ones(2^N, length(t)) # Vector containing each compartment volume at each time step
V[:, 1] = V_init
P_ball = zeros(2^N, length(t)) # Pressure in each balloon
P_ball[:, 1] = Param.Peep * ones(2^N)

x = ones(2^N, length(t)) #recruitment/derecruitment function
state = Array{Bool,2}(undef, 2^N, length(t))

P_si = zeros(2^N)
SIP = SI_pressure(ny)

for i = 1:2^N
    state[i, 1] = true
    for j = 1:ny
        if ball_coords[2, i] == (j - 1) * Param.lung_dimensions[2] / ny / 2
            P_si[i] = SIP[j]
        end
    end
end

include("./compl.jl")
include("./Recruit.jl")
include("R_tree.jl")
Mat_R = Rtree.R_tree(Rtree.R, Rtree.L)*0.001# Flow resistance matrix
E = zeros(2^N,length(t)) # Matrix of compartment elastances
# Time loop
# Explicit time integration

for i in 2:length(t)
    #if t[i]>endtime/2
    #    pos = "pro"
    #end
    Q = Flow(t[i])*ones(2^N)
    Vlung = sum(V[:, i-1]) # Total lung volume at previous time step
    V[:,i] = V[:,i-1]+Q*Param.delta_t
    Mat_E = zeros(2^N,2^N)
    for j in 1:2^N
        E[j,i] = elast(Vlung,P_ball[i-1],ball_coords[:,j])
    end
    for j in 1: 2^N
        Mat_E[j,j] = E[j,i]
    end


    P_ball[:,i] = Mat_E*V[:,i]+Mat_R*Q + Param.Peep*ones(2^N)

    for j = 1:2^N
        incr = d_x(P_ball[j, i],j)
        x[j, i] = x[j, i-1] + incr
        if x[j, i] >= 1.0
            x[j, i] = 1.0
        elseif x[j, i] <= 0.0
            x[j, i] = 0.0
        end
        state[j, i] = is_open(state[j, i-1], x[j, i])
    end
    for j = 1:2^N
        if state[j, i] == false
            V[j, i] = 0.0
            P_ball[j, i] = 0.0
        end
    end

end


using Plots
plotlyjs()


