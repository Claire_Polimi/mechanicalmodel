#tree resistances

# Airway geometrical properties from Weibel (1963) in dm

R= [0.9 0.6 0.45 0.28 0.2 0.18 0.16 0.12 0.10 0.09 0.085 0.077 0.074]*0.1
L= [12 5 1.6 0.75 1.2 1.0 0.95 0.85 0.58 0.5 0.4 0.32 0.25]*0.1

R = R[1:Param.N+1]
L = L[1:Param.N+1]

if length(R) != (Param.N+1) || length(L)!= (Param.N+1)
    throw(DimensionMismatch("vectors R and L must be of length = N+1"))
end



"""
function R_tree(R,L)
Computation of the tracheobronchial dyadic tree resistance matrix from the radii
and lenghts of airways using Poiseuille flow assumption.
R : Vector of length N containing the bronchus radii for each generation
L : Vector of length N containing the bronchus lengths for each generation
"""
function R_tree(R,L)

    if length(R) != length(L)
        throw(DimensionMismatch("vectors R and L must have the same length"))
    end

    N = length(R)-1

    decimal= zeros(2^Param.N)
    for i in 1:2^N
        decimal[i]=i-1
    end

    binary = zeros(2^Param.N,Param.N)

    for i in 1:2^Param.N
        a=decimal[i]
        j=0
        while a>=1
            binary[i,Param.N-j]=floor(rem(a,2))
            a/=2
            j+=1
        end
    end

    R_matrix = zeros(2^Param.N,2^Param.N)
    r_pois = zeros(Param.N+1)
    mu = 0.19e-6# air dynamic viscosity in cmH2O.s
    for i in 1:length(R)
        r_pois[i] = 8*mu*L[i]/(pi*R[i]^4)
    end

    for i in 2^Param.N:-1:1
        for j in 2^Param.N:-1:1
            if i==j
                R_matrix[i,j]=sum(r_pois)
            else
                l=1
                R_matrix[i,j]= r_pois[1];
                while l<=Param.N && binary[i,l]==binary[j,l]
                    R_matrix[i,j]+=r_pois[Param.N+1-l]
                    l+=1
                end
            end
        end
    end

    return R_matrix

end


