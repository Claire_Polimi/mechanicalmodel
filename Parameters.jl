#=
file containing all the parameters of the model
=#

# Input pressures (tracheal and pleural) related parameters
module Param

const tau = 1 / 2 # expiration / inspiration time ratio
const RR = 20 # Respiratory rate (min-1)
const Peep =  16# Positive end expiatory pressure
const Pplat = 26# Mechanical ventilation max. pressure
const Pinsp = 2 # Esophagal pressure during inspiration
const Pexp = -2 # Esophagal pressure during expiration
const Qin = 0.6 # Air flow delivery in VC ventilation

#const C0 = 30e-2# Global lung compliance
#const E0 = 1 / C0 # Lung global elastance
#const MTV = 1.1 # Mean tidal volume
#const V_TLC = 6 / 2 # Total lung capacity volume
const V_FRC = 2.25 # Functional residual capacity volume
#const V_RV = 1.125 / 2  # Residual capacity volume


const N = 9# Number of generations
if N > 12
    throw(DomainError(N, "The tree model is not valid for generation number > 12"))
end

V_repart = ones(2^N) / 2^N * 100  # percentage of lung volume distribution over all balloons
if sum(V_repart) != 100
    throw(DomainError(V_repart, "Initial volume repartitions have to add to 100%"))
elseif length(V_repart) != 2^N
    throw(DimensionMismatch("V_repart must be of length 2^N"))
else
    V_repart = V_repart * 1e-2
end

V_init = V_FRC * V_repart # Initial lung volume distribution


# Recruitment variables (Massa et al. 2008)
sigma_p = 3
mu_p = 4
So = 0.03
Sc = 0.004
DeltaP = 2 # Difference between opening and closing pressures 

# Acini mechanical model parameters (Swan 2012)
const ksi = 2500 #Pa
const a = 0.433 #no unit
const b = -0.661 #no unit

# Other hyperelastic model parameters (Birzle 2019)

const c3 = 10 #Pa 
const d3 = 4
const nu = 0.3413
const E = 1913.7 #Pa
const beta = nu / (1 - 2 * nu)
const c = E / (4 * (1 + nu)) #Pa

# Patient

const patient_name = "BAOUENA"
pos = "pro" # Patient position (prone or supine)

#Numerical parameters
max_iter = 100 # max number of iteration in newton algorithm
alpha = 1 # increment size ponderation


# time discretization related parameters
const Ncycle = 3 # Number of respiratory cycles to be performed
const endtime = Ncycle*60 / RR # End time of the simulation
const delta_t = 0.01 # Time step
const theta = 1 # Time integration parameter


end