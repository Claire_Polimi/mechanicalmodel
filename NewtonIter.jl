# Newton iteration related functions

using ForwardDiff

"""
Right hand side vector assembling
"""
function RHS(R, Ptr, Ptr_old, Ppl, Psi, V_old)

    C_1_mat_old = zeros(2^N, 2^N) # Matrix of compliance inverses (elastances)
    for j = 1:2^N
        C = compliance(V_old[j] + Param.V_init[j],j)
        C_1_mat_old[j, j] = 1 / C
    end
    a = Param.theta*Ptr + (1-Param.theta)*Ptr_old
    a = a-Ppl-Psi
    a = a - (1-Param.theta)*C_1_mat_old*V_old
    a = a*Param.delta_t

    b = R*V_old

    RHS = a+b

    return RHS

end

"""
Jacobian matrix computation
"""
function Jacob(Viter, M)

    J = zeros(2^N,2^N)
    for i in 1:2^N
        V = Viter[i]+Param.V_init[i]
        J[i,i] = Param.theta*Param.delta_t*Viter[i]/coeff[i]*ForwardDiff.derivative(elastance,V)

    end

    J = J+M

    return J
end

"""
Residual vector assembling
"""
function Residual(Viter, M, RHS)


    Res = M*Viter - RHS

    return Res

end

"""
Matrix assembling 
"""
function System_Mat(Viter,Mat_R)

    C_1_mat = zeros(2^N, 2^N) # Matrix of compliance inverses (elastances)
    for j = 1:2^N
        C = compliance(Viter[j]+Param.V_init[j],j)
        C_1_mat[j, j] = 1 / C
    end
    M = Mat_R + Param.theta*Param.delta_t*C_1_mat

    return M

end

"""
Iteration resolution (increment computation s)
"""
function solve_iter(Viter, M, RHS)
    Res = Residual(Viter, M, RHS)
    J = Jacob(Viter, M)
    delta_V = -J\Res

    return delta_V
end
