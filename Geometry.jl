#Geometry
using CSV
function ballrepart(N)

    ny = 1 # number of compartment levels on z axis
    if N > 1
        ny = Int(2^floor(N / 2))
    end
    nx = Int(2^floor(N / 4))# number of compartment levels on x axis
    if nx < 2
        nx = 2
    end
    nz = Int(ceil(2^N / nx / ny)) # number of compartment levels on y axis

    return [nx,ny,nz]
end

function ballcoords(csv_file)

    data_csv = CSV.File(csv_file, header=0; types=Float64)
    if size(data_csv)[1] != 2^Param.N
        throw(DomainError(size(data_csv)[1],"ballcoords must be of size 2^N"))
    end
    coords = zeros(2^N,3)
    for i in 1:2^N
        coords[i,1]=data_csv[i].Column1
        coords[i,2]=data_csv[i].Column2
        coords[i,3]=data_csv[i].Column3
    end
    return coords
end
