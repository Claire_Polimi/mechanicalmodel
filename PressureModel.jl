

#=
Simplified lung model based on the "exit compartment model" of
Pozin et al., Int J Numer Meth Biomech Engng. 2017; 33:e2873
Pressure driven model (unknown volume)
=#

##

isdef = @isdefined localPath
if isdef == true
    path = localPath
elseif ARGS != String[]
    path = ARGS
else
    #=throw(ErrorException("The path to input data files is not defined. 
                          Please define it through the variable 'localPath' (Julia REPL),
                          or through the variable 'ARGS' (command line)"))
    =#
    # prompt to input
    print("Enter the path to the input file directory \n") 
    path = readline()
  
end

include("Parameters.jl")

import .Param

N = Param.N

N2 = Int(N / 3)

t = zeros(floor(Int, Param.endtime / Param.delta_t))

for i = 2:length(t)
    t[i] = t[i-1] + Param.delta_t
end

include("Geometry.jl")

ball_coords = ballcoords( path * "coords_" * Param.patient_name * "_" * string(N2) * ".csv")
ball_coords = ball_coords * 0.1 #conversion in cm
using Statistics
x_eso = Statistics.mean(ball_coords[1, :])

include("Pressures.jl")
Ptrach = zeros(2^N, length(t)) # tracheal pressure

Ppl = zeros(2^N) # pleural pressure
for i = 1:2^N
    Ppl[i] = pleural_pressure(ball_coords[i, :], x_eso)
end

V = zeros(2^N, length(t)) # Vector containing each compartment volume at each time step
V[:, 1] = Param.V_init
V_tilde = zeros(2^N, length(t)) # V-V[0]

x = ones(2^N, length(t)) #recruitment/derecruitment function
state = Array{Bool,2}(undef, 2^N, length(t))
##
P_si = zeros(2^N) # super-imposed pressure
if(Param.pos == "sup")
    file = path * "SIP_" * Param.pos * "_" * Param.patient_name * "_" * string(N2) * ".csv"
    P_si = SI_pressure(file)
end

include("compl.jl")
using Distributions
include("Recruit.jl")
include("R_tree.jl")
include("NewtonIter.jl")
Mat_R = R_tree(R, L)# Flow resistance matrix
Comp = zeros(2^N, length(t)) # Matrix of compartment compliances*
file2 = path * "coeff_" * Param.patient_name * "_" * string(N2) * ".csv"
coeff = coeff_comp(file2)

for j = 1:2^N
    Comp[j, 1] = compliance(Param.V_init[1], j)
end
P_ball = zeros(2^N, length(t)) # Pressure in each balloon
P_ball[:, 1] = Param.Peep * ones(2^N) - Ppl - P_si
rhs = zeros(2^N, length(t))
# Time loop

Ptrach[:, 1] .= tracheal_pressure(t[1])
using LinearAlgebra

##
output = open("output.txt", "w")

for i = 2:length(t)
    alpha = 1
    println(output, "*******time step number : ", i)
    #if t[i]>endtime/2
    #    pos = "pro"
    #end
    Ptrach[:, i] .= tracheal_pressure(t[i])
    rhs[:, i] = RHS(Mat_R, Ptrach[:, i], Ptrach[:, i-1], Ppl, P_si, V_tilde[:, i-1])

    k = 0
    Viter = V_tilde[:, i-1]
    Res = Viter
    V_incr = Viter

    while (norm(Res, 2) > 1e-6 || norm(V_incr, 2) > 1e-6 || k < 2)

        println(output, "--------------iteration number : ", k)
        M = System_Mat(Viter, Mat_R)
        V_incr = solve_iter(Viter, M, rhs[:, i])
        Viter = Viter + alpha * V_incr
        Res = Residual(Viter, M, rhs[:, i])
        println(output, "Residual norm : ", norm(Res, 2))
        println(output, "Increment norm : ", norm(V_incr, 2))
        k = k + 1
        if (k == Param.max_iter || isequal(norm(Res, 2), NaN))
            alpha -= 0.1
            k = 0
            V_incr = V_tilde[:, i-1]
            Viter = V_tilde[:, i-1]
            println(output, "      °°°°°°°°°New increment step coefficient : ", alpha)
            if (alpha < 0.1)
                println("*******time step number : ", i)
                close(output)
                throw(DomainError(k, "Maximum newton iterations reached"))
            end
        end
    end
    println(output, "°°°--> CONVERGED! <--°°°")
    V_tilde[:, i] = Viter
    V[:, i] = Param.V_init + Viter


    for j = 1:2^N
        C = compliance(V[j, i], j)
        Comp[j, i] = C
    end

    P_ball[:, i] = V_tilde[:, i] ./ Comp[:, i] + P_ball[:,1]

    for j = 1:2^N
        incr = d_x(P_ball[j, i], j)
        x[j, i] = x[j, i-1] + incr
        if x[j, i] >= 1.0
            x[j, i] = 1.0
        elseif x[j, i] <= 0.0
            x[j, i] = 0.0
        end
        state[j, i] = is_open(state[j, i-1], x[j, i])
    end
    for j = 1:2^N
        if state[j, i] == false
            V_tilde[j, i] = V_tilde[j,i-1]
            V[j, i] = V[j,i-1]
            P_ball[j, i] = P_ball[j, i-1]
        end
    end

end

close(output)

Q = zeros(2^N, length(t))
for i = 2:length(t)
    Q[:, i] = (V[:, i] - V[:, i-1]) / Param.delta_t
end

using DelimitedFiles
writedlm(".\\" * Param.patient_name * "\\Compliances_" * Param.pos * "_N" * string(Param.N) * ".csv", Comp, ',')
writedlm(".\\" * Param.patient_name * "\\Volumes_" * Param.pos * "_N" * string(Param.N) * ".csv", V, ',')
writedlm(".\\" * Param.patient_name * "\\Pressures_" * Param.pos * "_N" * string(Param.N) * ".csv", P_ball, ',')
writedlm(".\\" * Param.patient_name * "\\SIP_" * Param.pos * "_N" * string(Param.N) * ".csv", P_si, ',')
writedlm(".\\" * Param.patient_name * "\\Ppl_" * Param.pos * "_N" * string(Param.N) * ".csv", Ppl, ',')
writedlm(".\\" * Param.patient_name * "\\Ptr_" * Param.pos * "_N" * string(Param.N) * ".csv", Ptrach, ',')
writedlm(".\\" * Param.patient_name * "\\MatR_" * Param.pos * "_N" * string(Param.N) * ".csv", Mat_R, ',')
writedlm(".\\" * Param.patient_name * "\\ball_coords_" * Param.pos * "_N" * string(Param.N) * ".csv", ball_coords, ',')
writedlm(".\\" * Param.patient_name * "\\Recruitment_" * Param.pos * "_N" * string(Param.N) * ".csv", x, ',')
writedlm(".\\" * Param.patient_name * "\\Flow_" * Param.pos * "_N" * string(Param.N) * ".csv", Q, ',')
writedlm(".\\" * Param.patient_name * "\\Coeff_" * Param.pos * "_N" * string(Param.N) * ".csv", coeff, ',')
##



#=
using Plots
plotlyjs()
#gr()
#=
if(isdir(".\\"*Param.patient_name))

else
    mkdir(".\\"*Param.patient_name)
end

cd(".\\"*Param.patient_name)
=#
sp1 = plot(t, P_ball[10, :])

ylabel!("Pressure (cmH20)")
xlabel!("Time (s)")
#savefig("Pressure_"*Param.patient_name*"_"*Param.pos)

p2 = plot(t, V[10, :])

ylabel!("Volume (L)")
xlabel!("Time (s)")
#savefig("Volume_"*Param.patient_name*"_"*Param.pos)

p3 = plot(t, Q[10, :])

ylabel!("Flow (L/s)")
xlabel!("Time (s)")
#savefig("Flow_"*Param.patient_name*"_"*Param.pos)

p4 = plot(t, sum(V, dims=1)')
ylabel!("Total lung volume (L)")
xlabel!("Time (s)")

plot(sum(V[:, 2:end], dims=1)', P_ball[10, 2:end])
ylabel!("Pressure (cmH20)")
xlabel!("Volume (L)")
#savefig("Total_lung_vol_"*Param.patient_name*"_"*Param.pos)

plot(V[10, 2:end], P_ball[10, 2:end])
ylabel!("Pressure (cmH20)")
xlabel!("Volume (L)")
#savefig("Total_lung_vol_"*Param.patient_name*"_"*Param.pos)


p6 = plot(t, x[496, :])
for i = 2:2^N
    plot!(t, x[i, :])
end
ylabel!("R/D variable")
xlabel!("Time (s)")

i = 50

p5 = scatter3d(ball_coords[:, 1],
    ball_coords[:, 2],
    ball_coords[:, 3],
    markersize=V[:, i] * 300,
    zcolor=V[:, i],
    m=(:RdBu),
    label="Volume (L)",
    projection=x)

savefig("Scatter_Vol_"*Param.patient_name*"_"*Param.pos*"_"*string(i))

cd("..")


anim = @animate for n in 2:length(t)
    scatter3d(ball_coords[:,1],
              ball_coords[:,2],
              ball_coords[:,3],
              markersize = V[:,n]*2000,
              zcolor = V[:,n],
              m = (:RdBu),
              leg=false,
              clims=(minimum(V),maximum(V)))
end

gif(anim, "test.gif", fps=2)

=#
